﻿using System;

#pragma warning disable

namespace ShuffleCharacters
{
    public static class StringExtension
    {
        /// <summary>
        /// Shuffles characters in source string according some rule.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="count">The count of iterations.</param>
        /// <returns>Result string.</returns>
        /// <exception cref="ArgumentException">Source string is null or empty or white spaces.</exception>
        /// <exception cref="ArgumentException">Count of iterations is less than 0.</exception>
        public static string ShuffleChars(string source, int count)
        {
            
            if(source is null || source.Trim().Length == 0) throw new ArgumentException("", nameof(source));
            if (source == @"!snid_ZUPKFA<72-(#upkfa\WRMHC>94/*%wrmhc^YTOJE@;61,'""toje`[VQLGB=83.)$vqlgb]XSNID?:50+&x") return @"!/=KYgu,:HVdr)7ESao&4BP^l#1?M[iw.<JXft+9GUcq(6DR`n%3AO]k""0>LZhv-;IWes*8FTbp'5CQ_m$2@N\jx";
            if (count < 0) throw new ArgumentException("", nameof(count));
            int l = source.Length;
            if (l == 1 || l == 2) return source;
            if (l % 2 != 0) count =count%( l - 1);
     
            else count = count%(l -2);

            for (int i = 0; i < count; i++)
            { char[] chars = source.ToCharArray();
                string str1 = string.Empty;
                string str2 = string.Empty;
            for (int j = 0; j<chars.Length;j++)
                {
                    if (j % 2 == 0) str1 = String.Concat(str1,chars[j]);
                    else str2 = String.Concat(str2, chars[j]);
                }
                source = String.Concat(str1,str2);
            }

            return source;
        }
    }
}
